from collections import namedtuple
from pathlib import Path
from sys import argv, exit as sys_exit

import html5lib

Element = namedtuple('Element', ['name', 'data', 'children'])

def get_element(token):
    ''' Get Element namedtuple from token dictionary '''
    name, data = token['name'], token['data']

    return Element(name, data, [])

def parse_fragment_tokens(tokens):
    ''' Parse start and end tags into nested structure of Elements and tokens '''
    root = Element('root', {}, [])

    element_stack = [root]

    for token in tokens:
        token_type = token['type']

        if token_type == 'StartTag':
            new_element = get_element(token)

            element_stack[-1].children.append(new_element)

            element_stack.append(new_element)
        elif token_type == 'EndTag': 
            tag_name = token['name']

            last_element = element_stack.pop()

            assert(last_element.name == tag_name)
        elif token_type == 'SpaceCharacters':
            continue
        else:
            element_stack[-1].children.append(token)

    return root.children

def render_element(element, indent='  ', depth=0):
    ''' Record an Element and its children to the document '''
    parts = []

    line_indent = indent * depth

    if element.data:
        attribute_string = ' '.join([
            f'{key[1]}="{value}"' if value else key[1] 
            for key, value in element.data.items()
        ])

        parts.append(f"{line_indent}<{element.name} {attribute_string}>")
    else:
        parts.append(f"{line_indent}<{element.name}>")

    if element.children:
        # If the singular child is a single line string, do not put on new line
        if len(element.children) == 1 and not isinstance(element.children[0], Element) and element.children[0]['type'] == 'Characters' and '\n' not in element.children[0]['data'].strip():
            string = render_token(element.children[0], single_line=True)

            parts.append(string.strip())

            parts.append(f"</{element.name}>\n")
        else:
            parts.append('\n')

            parts.append(render_children(element.children, indent, depth=depth + 1))

            parts.append(f"{line_indent}</{element.name}>\n")
    else:
        parts.append(f"</{element.name}>\n")

    return ''.join(parts)

def render_token(token, indent='  ', depth=0, single_line=False):
    ''' Print a token (which was not converted to an element) '''
    parts = []

    token_type = token['type']

    line_indent = indent * depth

    if token_type == 'EmptyTag':
        tag_name, tag_data = token['name'], token['data']

        if tag_data:
            attribute_string = ' '.join([
                f'{key[1]}="{value}"' if value else key[1] 
                for key, value in tag_data.items()
            ])

            parts.append(f"{line_indent}<{tag_name} {attribute_string}>\n")
        else:
            parts.append(f"{line_indent}<{tag_name}>\n")
    elif token_type == 'Comment':
        comment_data = token['data']

        # If multiple lines...
        if '\n' in comment_data:
            parts.append(f"{line_indent}<!--{comment_data}{line_indent}-->\n")
        else:
            parts.append(f"{line_indent}<!--{comment_data}-->\n")
    elif token_type == 'Characters':
        string = token['data']
        parts.append(f"{line_indent}{string}\n")
    else:
        raise ValueError(f"Token type '{token_type}' not supported for printing")

    return ''.join(parts)

def render_children(children, indent='  ', depth=0):
    ''' Forward children to appropiate print functions '''
    def render_child(child):
        if isinstance(child, Element):
            return render_element(child, indent, depth=depth)
        else:
            return render_token(child, indent, depth=depth)

    return ''.join([render_child(child) for child in children])

def main():
    # try:
    #     path = Path(argv[1])
    # except IndexError:
    #     sys_exit('Please provide a file path')

    parser = html5lib.HTMLParser()

    # document = parser.parseFragment(path.read_text())

    walker = html5lib.getTreeWalker("etree")

    # # for token in walker(document):
    # #     print(token)
    # #     input()

    # rendered_document = render_children(parse_fragment_tokens(walker(document)), indent='    ')

    # path.write_text(rendered_document, encoding='utf-8')

    for path in Path(argv[1]).glob('*.html'):
        print('Processing:', path)

        source_document = path.read_text(encoding='utf-8').replace('\ufeff\n', '')

        rendered_document = render_children(parse_fragment_tokens(walker(parser.parseFragment(source_document))), indent='    ')

        path.write_text(rendered_document, encoding='utf-8')
